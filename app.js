const http = require('http');
const fs = require('fs');
const path = require('path');
const temperature = require('./api/temperature.js');
const light = require('./api/light.js');
const rbCommunication = require('./api/raspberry_communication.js');
const config = require('./config.json');

const PORT = config.PORT;
const CONNECT_RASPBERRY = config.CONNECT_RASPBERRY;
const RASPBERRY_PORT = config.RASPBERRY_PORT;

let USE_MOCK = config.DEFAULT_USE_MOCK;

console.log('Server running at http://127.0.0.1:' + PORT + '/');

function initialize() {
    temperature.startMonitor(1000, USE_MOCK, () => setUseMock(true));
    light.initialize(USE_MOCK, () => setUseMock(true));

    http.createServer(function (request, response) {
        try {
            if (request.url.startsWith('/api')) {
                handleApi(request, response);
            } else {
                handleFile(request, response);
            }
        } catch (error) {
            console.log(error);
            response.writeHead(500);
            response.end();
        }
    }).listen(PORT);
}

function handleApi(request, response) {
    if (request.url.startsWith('/api/getUseMockData')) {
        response.writeHead(200, { 'Content-Type': 'application/json' });
        response.end(JSON.stringify({ value: USE_MOCK }));
    } else if (request.url.startsWith('/api/setUseMockData')) {
        setUseMock(request.url.endsWith('true'));

        response.writeHead(200);
        response.end();
    } else if (request.url.startsWith('/api/temperature')) {
        temperature.handleRequest(request, response);
    } else if (request.url.startsWith('/api/light')) {
        light.handleRequest(request, response);
    } else {
        response.writeHead(404);
        response.end();
    }
}

function handleFile(request, response) {
    let filePath = '.' + request.url;
    if (filePath == './')
        filePath = './index.html';

    const extname = path.extname(filePath);
    let contentType = 'text/html';
    switch (extname) {
        case '.js':
            contentType = 'text/javascript';
            break;
        case '.css':
            contentType = 'text/css';
            break;
        case '.json':
            contentType = 'application/json';
            break;
        case '.png':
            contentType = 'image/png';
            break;      
        case '.jpg':
            contentType = 'image/jpg';
            break;
        case '.wav':
            contentType = 'audio/wav';
            break;
        case '.svg':
            contentType = 'image/svg+xml';
            break;
    }

    fs.readFile(filePath, function(error, content) {
        if (error) {
            if(error.code == 'ENOENT'){
                fs.readFile('./404.html', function(error, content) {
                    response.writeHead(200, { 'Content-Type': contentType });
                    response.end(content, 'utf-8');
                });
            }
            else {
                response.writeHead(500);
                response.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
                response.end(); 
            }
        }
        else {
            response.writeHead(200, { 'Content-Type': contentType });
            response.end(content, 'utf-8');
        }
    });
}

function setUseMock(useMock) {
    USE_MOCK = useMock;
    console.log('USE_MOCK: ' + USE_MOCK);

    temperature.setUseMock(USE_MOCK);
    light.setUseMock(USE_MOCK);
}

if (CONNECT_RASPBERRY) {
    rbCommunication.startCommunication(RASPBERRY_PORT, () => setUseMock(true))
        .then(() => initialize());
} else {
    initialize();
}