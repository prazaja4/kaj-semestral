# IoT Monitor

## Ukázka

Video s ukázkou s reálným zařízením: https://youtu.be/t2JA--cF3Hc

## Backend

Backend je napsán v **Node.js**. Před spuštěním je potřeba mít nainstalované potřebné knihovny, konkrétně *sqlite3*, *serialport* a *async-mutex*. Pro nainstalování stačí zavolat `npm install` v kořenovém adresáři (obsahujícím **app.js**). 

Pro spuštění backendu musí být uživatel v kořenovém adresáři (obsahujícím **app.js**) a použít příkaz `node app.js`. Server se spustí na výchozí adrese **http://localhost:3000**.

Nastavení backendu se provádí v souboru **config.json**. Lze nastavit následující parametry:
- **PORT**: číslo portu, na kterém se spustí server.
- **DEFAULT_USE_MOCK**: výchozí hodnota pro používání mockovaných dat, tj. používání náhodně generovaných dat pro teplotu namísto čtení dat z fyzického zařízení.
- **CONNECT_RASPBERRY**: zda se má server zkoušet připojit k Raspberry Pi.
- **RASPBERRY_PORT**: pokud je **CONNECT_RASPBERRY** nastaveno na **true**, bude se server zkoušet připojit k Raspberry Pi na tomto portu.

Skripty starající se o jednotlivé části backendu jsou ve složce **api**.

## Frontend

### Codebase

Frontend je vytvořen jako single-page aplikace. Veškeré HTML je v souboru **index.html**. Styly se nacházejí ve složce **styles** a jsou rozdělené do několika souborů dle komponenty, ke které přísluší. Podobně JavaScriptové soubory jsou rozděleny dle příslušnusti k jednotlivým komponentám do ES6 modulů ve složce **modules**. Další zdroje (obrázky) jsou ve složce **resources**.

### Aplikace

Po příchodu na stránku je uživateli vykreslen graf s naměřenými teplotami za posledních 60 vteřin s intervalem 1 vteřina mezi dvěma hodnotami. Nad grafem je vypsána poslední naměřená hodnota a spočten průměrná teplota za zobrazené období. Délku zobrazeného období lze změnit dropdown nabídkou nad pravou částí grafu. Aktuální pohled a graf lze také exportovat ve formátu PNG kliknutím na tlačítko *Export*.

Na úvodní stránce je také zobrazen "tachometr" ukazující aktuální jas LEDky na zařízení. Jas lze měnit klikáním na stupnici tachometru (červený oblouk). Svítivost LED je také indikovaná zelenou žárovkou u tachometru.

V pravém horním rohu obrazovky se může uživatel kliknutím na ozubené kolečko přesunout do nastavení. Zde lze měnit hodnoty třech parametrů:
- **Client-side refresh rate**: mění interval (v sekundách) mezi dvěma hodnotami vykreslenými v grafu. Je vhodné, aby uživatel tuto hodnotu zvýšil, když zvýší délku zobrazované historie u grafu teploty, aby byly lépe vidět jednotlivé hodnoty.
- **Server-side refresh rate**: určuje, po jaké době (v milisekundách) se server doptává zařízení na nový údaj o teplotě. Nižší hodnota znamená vyšší vytížení zařízení kvůli zvýšené komunikace a také rychlejší plnění databáze.
- **Use mock data**: nastavuje, zda má server využívat mockovací data (teplotu a svítivost) nebo číst reálná data ze zařízení. Pokud uživatel nastaví hodnotu na **NO** a serveru se nepodaří komunikaci se zařízením uskutečnit (například když zařízení není připojené), nastaví hodnotu zpět na **YES**.

Zadaná hodnota se potvrdí stiskem tlačítka *Apply*, při zadání neplatné hodnoty nebo při chybě v komunikaci se server je vypsána chybová hláška. Aktuálně nastavená hodnota parametru je uvedena pod jeho názvem (druhé dva parametry mohou být měněny jinými uživateli).

Hodnoty týkajícího se klientského nastavení aplikace, tzn. délka historie zobrazování teplot a interval mezi teplotami, se ukládají na klientské straně do **LocalStorage**, takže se automaticky aplikují při příštím přístupu na stránku.

## Raspberry Pi Pico W

Kód běžící na Raspberry Pi Pico W v prostředí MicroPythonu je v souboru **raspberry/boot.py**.