export async function startLightMonitoring() {
    SCALE = document.querySelector('#lightInformation .tachometerScale');
    POINTER = document.querySelector('#lightInformation .tachometerPointer');
    CENTER = document.querySelector('#lightInformation .tachometerCenter');
    TEXT = document.querySelector('#lightInformation .tachometerTextValue');
    LIGHT = document.querySelector('#lightInformation .lightbulbIcon');

    SCALE.addEventListener('pointermove', handleScalePointerMove);
    SCALE.addEventListener('pointerdown', handleScalePointerMove);

    await getLighting();
    setInterval(getLighting, 1000);
}

let POINTER = null;
let SCALE = null;
let CENTER = null;
let TEXT = null;
let LIGHT = null;

async function handleScalePointerMove(e) {
    if (e.buttons !== 1) return;
    const centerX = CENTER.getBoundingClientRect().left + CENTER.getBoundingClientRect().width / 2;
    const centerY = CENTER.getBoundingClientRect().top + CENTER.getBoundingClientRect().height / 2;
    const angle = (Math.atan2(e.clientY - centerY, e.clientX - centerX) * 180 / Math.PI) + 180;
    setLighting(angle);
    await visualize(angle);
}

async function visualize(angle) {
    const minAngle = 205;
    const maxAngle = 335;
    if (angle > minAngle && angle < maxAngle) { return; }
    POINTER.style.rotate = `${angle}deg`;
    
    const percentage = toPercentage(angle);
    TEXT.innerText = percentage.toFixed(1);

    const lighting = (percentage + 1) / 101;
    LIGHT.style.opacity = lighting;
}

function toPercentage(angle) {
    const minAngle = 205;
    angle += 25;
    angle %= 360;
    const percentage = (angle / (minAngle + 25)) * 100;
    return percentage;
}

function setLighting(angle) {
    let percentage = toPercentage(angle);
    if (percentage > 100 || percentage < 0) { return; }
    fetch(`/api/light/setLighting?value=${percentage}`);
}

async function getLighting() {
    const response = await fetch('/api/light/getLighting');
    const json = await response.json();
    const value = json.value;

    const minAngle = 205;
    let angle = (value / 100) * (minAngle + 25);
    angle -= 25;

    await visualize(angle);
}
