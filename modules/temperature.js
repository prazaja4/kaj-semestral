import 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js';
import { LOCAL_STORAGE_TEMPERATURE_LAST_SECONDS_KEY } from './common.js';

export async function startTemperatureGraph(samplingPeriod, lastSeconds) {
    const temperatureHistorySelect = document.getElementById('temperatureHistorySelect');
    temperatureHistorySelect.value = getClosestHistoryValue(lastSeconds);
    temperatureHistorySelect.addEventListener('change', handleTemperatureHistorySelectChange);
    lastSeconds = temperatureHistorySelect.value * 60;

    console.log(samplingPeriod, lastSeconds);

    const exportButton = document.getElementById('temperatureExportButton');
    exportButton.addEventListener('click', handleExportButtonClick);

    const currentTemperatureLabel = document.getElementById('currentTemperatureValue');

    const averageTemperatureLabel = document.getElementById('averageTemperatureValue');
    
    STATE = new State(samplingPeriod, lastSeconds, exportButton, currentTemperatureLabel, averageTemperatureLabel);
    STATE.temperatures = await getTemperatureData(samplingPeriod, lastSeconds);
    setCurrentTemperatureLabel();
    setAverageTemperatureLabel();
    await drawTemperatureCanvas();
    
    STATE.lastTimeoutId = setTimeout(updateTemperatures, samplingPeriod * 1000);
}

function getClosestHistoryValue(seconds) {
    const minutes = seconds / 60;

    let best = null;
    let bestDiff = Number.MAX_SAFE_INTEGER;
    for (const option of temperatureHistorySelect.options) {
        const diff = Math.abs(option.value - minutes);
        if (diff < bestDiff) {
            best = option.value;
            bestDiff = diff;
        }
    }

    return best;
}

class State {
    constructor(samplingPeriod, lastSeconds, exportButton, currentTemperatureLabel, averageTemperatureLabel) {
        this.temperatures = [];
        this.exportButton = exportButton;
        this.currentTemperatureLabel = currentTemperatureLabel;
        this.averageTemperatureLabel = averageTemperatureLabel;
        this.samplingPeriod = samplingPeriod;
        this.lastSeconds = lastSeconds;
    }

    get sampleCount() {
        return Math.ceil(this.lastSeconds / this.samplingPeriod)
    }

    addTemperature(temperature) {
        this.temperatures.unshift(temperature);
        if (this.temperatures.length > Math.ceil(this.lastSeconds / this.samplingPeriod)) {
            this.temperatures.pop();
        }
    }
}

async function handleExportButtonClick(e) {
    const canvas = document.getElementById('temperatureChart');
    const dataUrl = canvas.toDataURL('image/png');
    const anchor = document.createElement('a');
    anchor.href = dataUrl;
    anchor.download = `temperature-${new Date().toISOString()}.png`;
    anchor.click();
}

async function getTemperatureData(samplingPeriod, lastSeconds = null) {
    lastSeconds ??= samplingPeriod;
    samplingPeriod *= 1000;
    lastSeconds *= 1000;
    const apiUrl = `/api/temperature/getTemperature?timeGap=${samplingPeriod}&lastMilliseconds=${lastSeconds}`;
    const response = await fetch(apiUrl, { signal: AbortSignal.timeout(samplingPeriod) });
    const data = await response.json();
    return data;
}

async function drawTemperatureCanvas() {
    const dataY = STATE.temperatures;
    const dataX = [];
    for (let i = 0; i < STATE.sampleCount; i++) {
        dataX.push(i * STATE.samplingPeriod);
    }
    STATE.chart = new Chart('temperatureChart', {
        type: 'line',
        data: {
            labels: dataX,
            datasets: [{
                backgroundColor: 'rgba(0,0,0,0.05)',
                borderColor: 'rgba(0,0,0,1.0)',
                data: dataY,

                lineTension: 0,

                pointRadius: 5,
                pointBackgroundColor: 'rgba(0,0,0,1.0)',
                pointHoverRadius: 6,
                pointHitRadius: 10
            }]
        },
        options: {
            legend: { display: false },
            maintainAspectRatio: false
        },
        plugins: [
            {
                beforeDraw: function(chart) {
                    const ctx = chart.chart.ctx;
                    ctx.fillStyle = 'white';
                    ctx.fillRect(0, 0, chart.chart.width, chart.chart.height);
                }
            }
        ]
    });
}

async function updateTemperatures() {
    const startTime = Date.now();
    try {
        const temperatures = await getTemperatureData(STATE.samplingPeriod);
        const temperature = temperatures[0];
        STATE.addTemperature(temperature);
        setCurrentTemperatureLabel();
    } catch (e) {
        STATE.addTemperature(null);
    }
    
    STATE.chart.update();
    setAverageTemperatureLabel();
    STATE.lastTimeoutId = setTimeout(updateTemperatures, (STATE.samplingPeriod * 1000) - (Date.now() - startTime));
}

function setCurrentTemperatureLabel() {
    STATE.currentTemperatureLabel.innerText = STATE.temperatures[0]?.toFixed(1) ?? Number.NaN;
}

function setAverageTemperatureLabel() {
    STATE.averageTemperatureLabel.innerText = (
        STATE.temperatures
            .filter(a => a != null)
            .reduce((a, b) => a + b, 0) / STATE.temperatures.filter(a => a != null).length)
        .toFixed(1);
}

async function handleTemperatureHistorySelectChange(e) {
    const selectedHistoryLength = parseInt(e.target.value);
    STATE.lastSeconds = selectedHistoryLength * 60;
    await redrawCanvas();

    localStorage.setItem(LOCAL_STORAGE_TEMPERATURE_LAST_SECONDS_KEY, STATE.lastSeconds);
}

export async function redrawCanvas() {
    clearTimeout(STATE.lastTimeoutId);
    STATE.temperatures = await getTemperatureData(STATE.samplingPeriod, STATE.lastSeconds);
    STATE.chart.destroy();
    await drawTemperatureCanvas();
    STATE.lastTimeoutId = setTimeout(updateTemperatures, STATE.samplingPeriod * 1000);
}

export let STATE = null;
