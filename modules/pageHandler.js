const PREVIOUS_PAGES = [];
let CURRENT_PAGE = null;

export function openPage(page) {
    PREVIOUS_PAGES.push(CURRENT_PAGE);
    CURRENT_PAGE.style.display = 'none';
    CURRENT_PAGE = page;
    CURRENT_PAGE.style.display = 'flex';
}

export function closePage() {
    if (PREVIOUS_PAGES.length === 0) {
        return;
    }
    CURRENT_PAGE.style.display = 'none';
    CURRENT_PAGE = PREVIOUS_PAGES.pop();
    CURRENT_PAGE.style.display = 'flex';
}

export function getCurrentPage() {
    return CURRENT_PAGE;
}

export function startPageHandler() {
    CURRENT_PAGE = document.querySelector('#firstPage');
}
