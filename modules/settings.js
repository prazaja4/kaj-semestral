import { openPage, closePage, getCurrentPage } from "./pageHandler.js";
import { STATE as TEMPERATURE_STATE, redrawCanvas as temperature_redrawCanvas } from "./temperature.js";
import { LOCAL_STORAGE_TEMPERATURE_CLIENT_REFRESH_RATE_KEY, LOCAL_STORAGE_TEMPERATURE_LAST_SECONDS_KEY } from "./common.js";

class State {
    constructor() {
        this.settingsPage = document.querySelector('#settingsPage');
        this.clientRefreshRateInput = document.getElementById('clientRefreshRateInput');
        this.clientRefreshRateCurrentValueLabel = document.getElementById('clientRefreshRateCurrentValueLabel');
        this.serverRefreshRateInput = document.getElementById('serverRefreshRateInput');
        this.serverRefreshRateCurrentValueLabel = document.getElementById('serverRefreshRateCurrentValueLabel');
        this.clientRefreshRateErrorLabel = document.getElementById('clientRefreshRateError');
        this.serverRefreshRateErrorLabel = document.getElementById('serverRefreshRateError');
        this.serverRefreshRateCommunicationFailedErrorLabel = document.getElementById('serverRefreshRateCommunicationFailedError');
        this.useMockDataInput = document.getElementById('useMockDataInput');
        this.useMockDataCurrentValueLabel = document.getElementById('useMockDataCurrentValueLabel');
        this.useMockDataCommunicationFailedErrorLabel = document.getElementById('useMockDataCommunicationFailedError');
    }
}

let STATE = null;

function toggleSettingsPage() {
    const currentPage = getCurrentPage();
    if (currentPage === STATE.settingsPage) {
        closePage();
    } else {
        openPage(STATE.settingsPage);
    }
}

async function handleClientRefreshRateApplyButtonClick(e) {
    try {
        const refreshRate = parseInt(STATE.clientRefreshRateInput.value);
        if (!Number.isInteger(refreshRate) || refreshRate < 1 || refreshRate > 60) {
            STATE.clientRefreshRateErrorLabel.style.display = 'block';
            STATE.clientRefreshRateInput.value = null;
            return;
        }

        if (refreshRate === TEMPERATURE_STATE.samplingPeriod) {
            STATE.clientRefreshRateInput.value = null;
            return;
        }

        TEMPERATURE_STATE.samplingPeriod = refreshRate;
        clientRefreshRateCurrentValueLabel.textContent = refreshRate;
        temperature_redrawCanvas();

        STATE.clientRefreshRateErrorLabel.style.display = null;

        localStorage.setItem('clientTemperatureRefreshRate', refreshRate);
    } catch (e) {
        console.warn(e);
    }

    STATE.clientRefreshRateInput.value = null;
}

async function handleServerRefreshRateApplyButtonClick(e) {
    try {
        const refreshRate = parseInt(STATE.serverRefreshRateInput.value);
        if (!Number.isInteger(refreshRate) || refreshRate < 100 || refreshRate > 60000) {
            STATE.serverRefreshRateErrorLabel.style.display = 'block';
            STATE.serverRefreshRateInput.value = null;
            return;
        }

        STATE.serverRefreshRateErrorLabel.style.display = null;

        const url = '/api/temperature/setRefreshRate?value=' + refreshRate;
        const response = await fetch(url);
        if (!response.ok) {
            STATE.serverRefreshRateCommunicationFailedErrorLabel.style.display = 'block';
        } else {
            STATE.serverRefreshRateCommunicationFailedErrorLabel.style.display = null;
            STATE.serverRefreshRateCurrentValueLabel.textContent = refreshRate;
        }
    } catch (e) {
        console.warn(e);
        STATE.serverRefreshRateCommunicationFailedErrorLabel.style.display = 'block';
    }

    STATE.serverRefreshRateInput.value = null;
}

async function handleUseMockDataApplyButtonClick(e) {
    const useMockData = STATE.useMockDataInput.checked;
    try {
        const url = '/api/setUseMockData?value=' + (useMockData ? 'true' : 'false');
        const response = await fetch(url);
        if (!response.ok) {
            STATE.useMockDataCommunicationFailedErrorLabel.style.display = 'block';
        } else {
            STATE.useMockDataCommunicationFailedErrorLabel.style.display = null;
            STATE.useMockDataCurrentValueLabel.textContent = useMockData ? 'YES' : 'NO';
        }
    } catch (e) {
        console.warn(e);
        STATE.useMockDataCommunicationFailedErrorLabel.style.display = 'block';
    }
}

export function loadSettings() {
    document.querySelector('#settingsIcon').addEventListener('click', toggleSettingsPage);
    STATE = new State();

    document.getElementById('clientRefreshRateApplyButton').addEventListener('click', handleClientRefreshRateApplyButtonClick);
    document.getElementById('serverRefreshRateApplyButton').addEventListener('click', handleServerRefreshRateApplyButtonClick);
    document.getElementById('useMockDataApplyButton').addEventListener('click', handleUseMockDataApplyButtonClick);

    setInterval(async () => {
        try {
            // Server refresh rate
            let response = await fetch('/api/temperature/getRefreshRate').then(response => response.json());
            const refreshRate = response.value;
            STATE.serverRefreshRateCurrentValueLabel.textContent = refreshRate;

            // Use mock data
            response = await fetch('/api/getUseMockData').then(response => response.json());
            const useMock = response.value;
            STATE.useMockDataCurrentValueLabel.textContent = useMock ? 'YES' : 'NO';
        } catch (e) {
            console.warn(e);
        }
    }, 1000);

    return restoreSettings();
}

function restoreSettings() {
    const refreshRate = localStorage.getItem(LOCAL_STORAGE_TEMPERATURE_CLIENT_REFRESH_RATE_KEY) ?? 1;
    if (refreshRate) {
        STATE.clientRefreshRateCurrentValueLabel.textContent = refreshRate;
    }

    const lastSeconds = localStorage.getItem(LOCAL_STORAGE_TEMPERATURE_LAST_SECONDS_KEY) ?? 60;
    return { refreshRate, lastSeconds };
}
