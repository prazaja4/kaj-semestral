import { startTemperatureGraph } from "./temperature.js";
import { loadSettings } from "./settings.js";
import { startPageHandler } from "./pageHandler.js";
import { startLightMonitoring } from "./light.js";

async function loadPage() {
    startPageHandler();
    const { refreshRate, lastSeconds } = loadSettings();
    startTemperatureGraph(refreshRate, lastSeconds);
    startLightMonitoring();
}

document.addEventListener('DOMContentLoaded', _ => loadPage());
