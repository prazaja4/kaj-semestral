const { SerialPort } = require('serialport');
const { Mutex } = require('async-mutex');

const COMMANDS = {
    NO_COMMAND: 0,
    SET_LIGHTING(value) {
        return Math.round(value + 1) & 0x7E;
    },
    GET_TEMPERATURE: 1,
    GET_LIGHTING: 7
}

module.exports.COMMANDS = COMMANDS;
module.exports.sendCommand = sendCommand;
module.exports.startCommunication = startCommunication;

let SERIAL_PORT = null;

const mutex = new Mutex();

async function sendCommand(command) {
    return await mutex.runExclusive(async () => {
        let result = null;
        if (command & 1) {
            result = await SERIAL_PORT.writeRead(command);
            result = result[0];
        } else {
            await SERIAL_PORT.write(command);
        }

        return result;
    });
}

async function startCommunication(port, errorCallback) {
    console.log(`Starting communication with Raspberry on port ${port}.`);

    try {
        SERIAL_PORT = new SerialPortPromise(port);
        await new Promise(resolve => setTimeout(resolve, 1000));
        mainLoop();
    } catch (error) {
        console.error(error);
        errorCallback();
    }
}

async function mainLoop() {
    while (true) {
        await sendCommand(COMMANDS.NO_COMMAND);
    }
}

class SerialPortPromise {
    #serialPort = null;

    constructor(port) {
        this.#serialPort = new SerialPort({
            path: port,
            baudRate: 115200
        });
        this.#serialPort.open(() => {
            console.log('Serial port connected.');
        })
    }
    
    read() {
        return new Promise((resolve, reject) => {
            this.#serialPort.once('data', data => {
                resolve(data);
            });
        });
    }

    write(byte) {
        return this.writeRead(byte);
    }

    writeRead(byte) {
        return new Promise((resolve, reject) => {
            this.#serialPort.write(Uint8Array.of(byte), err => {
                if (err) {
                    reject(err);
                }
                this.#serialPort.once('data', data => {
                    resolve(data);
                });
            });
        });
    }
}
