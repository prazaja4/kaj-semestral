const url = require('url');
const raspberry = require('./raspberry_communication.js');

let USE_MOCK = true;
let LIGHTING = 0.0;
let REAL_DATA_UNAVAILABLE_CALLBACK = null;

module.exports.initialize = function (useMock, realDataUnavailableCallback) {
    USE_MOCK = useMock;
    REAL_DATA_UNAVAILABLE_CALLBACK = realDataUnavailableCallback;

    checkLighting();
}

module.exports.setUseMock = function (useMock) {
    USE_MOCK = useMock;
    checkLighting();
}

module.exports.handleRequest = async function (request, response) {
    const urlPart = request.url.substring('/api/light'.length);
    const parsedUrl = url.parse(urlPart, true);

    if (urlPart.startsWith('/getLighting')) {
        response.writeHead(200, { 'Content-Type': 'application/json' });
        response.end(JSON.stringify({ value: LIGHTING }));
    } else if (urlPart.startsWith('/setLighting')) {
        try {
            const lighting = parseFloat(parsedUrl.query.value);
            if (!Number.isFinite(lighting) || lighting < 0 || lighting > 100) {
                throw new Error('Invalid lighting value.');
            }

            setLighting(lighting);

            response.writeHead(200);
            response.end();
        } catch (error) {
            console.error(error);
            response.writeHead(400);
            response.end();
        }
    } else {
        response.writeHead(404);
        response.end();
    }
}

async function checkLighting() {
    if (!USE_MOCK) {
        try {
            LIGHTING = await raspberry.sendCommand(raspberry.COMMANDS.GET_LIGHTING);
        } catch (error) {
            console.error(error);
            REAL_DATA_UNAVAILABLE_CALLBACK();
        }
    }
}

async function setLighting(value) {
    if (USE_MOCK) {
        LIGHTING = value;
    } else {
        try {
            await raspberry.sendCommand(raspberry.COMMANDS.SET_LIGHTING(value));
            LIGHTING = value;
        } catch (error) {
            console.error(error);
            REAL_DATA_UNAVAILABLE_CALLBACK();
        }
    }
}
