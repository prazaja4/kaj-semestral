const sql = require('sqlite3').verbose();
const url = require('url');
const raspberry = require('./raspberry_communication.js');

let REFRESH_RATE = 1000; // in milliseconds
let USE_MOCK = true;
let REAL_DATA_UNAVAILABLE_CALLBACK = null;

module.exports.startMonitor = async function (refreshRate, useMock, realDataUnavailableCallback) {
    REFRESH_RATE = refreshRate;
    USE_MOCK = useMock;
    REAL_DATA_UNAVAILABLE_CALLBACK = realDataUnavailableCallback;
    await createDb();
    insertTemperature();
}

module.exports.setUseMock = function (useMock) {
    USE_MOCK = useMock;
}

module.exports.handleRequest = async function (request, response) {
    const urlPart = request.url.substring('/api/temperature'.length);
    const parsedUrl = url.parse(urlPart, true);

    if (urlPart.startsWith('/getTemperature')) {
        const lastMilliseconds = parseInt(parsedUrl.query.lastMilliseconds);
        const timeGap = parseInt(parsedUrl.query.timeGap);
        getTemperatures(timeGap, lastMilliseconds, temperatures => {
            response.writeHead(200, { 'Content-Type': 'application/json' });
            response.end(JSON.stringify(temperatures));
        });
    } else if (urlPart.startsWith('/getRefreshRate')) {
        response.writeHead(200, { 'Content-Type': 'application/json' });
        response.end(JSON.stringify({ value: REFRESH_RATE }));
    } else if (urlPart.startsWith('/setRefreshRate')) {
        try {
            const refreshRate = parseInt(parsedUrl.query.value);
            if (!Number.isInteger(refreshRate) || refreshRate < 100 || refreshRate > 60000) {
                throw new Error('Invalid refresh rate');
            }

            REFRESH_RATE = refreshRate;
            console.log('REFRESH_RATE: ' + REFRESH_RATE);

            response.writeHead(200);
            response.end();
        } catch (error) {
            response.writeHead(400);
            response.end();
        }
    } else {
        response.writeHead(404);
        response.end();
    }
}

function openDb() {
    const db = new sql.Database(__dirname + '/../temperature.db', (err) => {
        if (err) {
            throw new Error(err.message);
        }
    });
    return db;
}

async function createDb() {
    const db = openDb();
    await new Promise(resolve => setTimeout(resolve, 100));

    db.run('CREATE TABLE IF NOT EXISTS temperature (id INTEGER PRIMARY KEY AUTOINCREMENT, temperature REAL, timestamp INTEGER)');
    await new Promise(resolve => setTimeout(resolve, 100));

    db.close();
}

async function readTemperature() {
    if (USE_MOCK) {
        return { 
            temperature: 21 + Math.random() * 2,
            timestamp: Date.now()
        };
    } else {
        try {
            const temperature = await raspberry.sendCommand(raspberry.COMMANDS.GET_TEMPERATURE);
            return {
                temperature: temperature,
                timestamp: Date.now()
            }
        } catch (error) {
            console.log(error);
            REAL_DATA_UNAVAILABLE_CALLBACK();
            return readTemperature();
        }
    }
}

async function insertTemperature() {
    const { temperature, timestamp } = await readTemperature();
    const db = openDb();
    db.run('INSERT INTO temperature (temperature, timestamp) VALUES (?, ?)', [temperature, timestamp]);
    db.close();
    setTimeout(insertTemperature, REFRESH_RATE);
}

async function getTemperatures(timeGap, lastMilliseconds, callback) {
    const until = Date.now() - lastMilliseconds*2;
    const sql = 'SELECT temperature, timestamp FROM temperature WHERE timestamp >= ? ORDER BY timestamp DESC';
    const db = openDb();
    db.all(sql, [until], (err, allRows) => {
        if (err) {
            throw new Error(err.message);
        }

        const numRows = Math.min(lastMilliseconds / timeGap, allRows.length);
        const rows = [];
        for (let i = 0; i < numRows; i++) {
            const timestamp = Date.now() - i * timeGap;
            const row = allRows.reduce((prev, curr) => Math.abs(curr.timestamp - timestamp) < Math.abs(prev.timestamp - timestamp) ? curr : prev).temperature;
            rows.push(row);
        }

        callback(rows);
    });
    db.close();
}
