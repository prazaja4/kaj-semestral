import machine
import sys

led = machine.Pin('LED', machine.Pin.OUT)
adc = machine.ADC(4)

LED_ON_TIME = 10
LED_FREQUENCY = 100
LED_COUNTER = 0

SERIAL_COUNTER = 0
SERIAL_FREQUENCY = 500

def blink():
    global LED_COUNTER
    LED_COUNTER += 1
    
    if LED_COUNTER < LED_ON_TIME:
        led.value(1)
    else:
        led.value(0)
    
    LED_COUNTER %= LED_FREQUENCY

def handleCommand():
    global LED_ON_TIME

    read = sys.stdin.read(1)
    read = ord(read)

    if read == 7: # Get lighting
        print(chr(LED_ON_TIME), end='')
    elif read == 1: # Get temperature
        ADC_voltage = adc.read_u16() * (3.3 / (65535))
        temperature = 27 - (ADC_voltage - 0.706) / 0.001721
        print(chr(int(temperature)), end='')
    elif read != 0: # Set lighting
        LED_ON_TIME = read - 1
        print(chr(0), end='')
    else:
        print(chr(0), end='')

while True:
    blink()

    if SERIAL_COUNTER == 0:
        handleCommand()

    SERIAL_COUNTER += 1
    SERIAL_COUNTER %= SERIAL_FREQUENCY
